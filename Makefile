MD := $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))")
BUILD = $(shell git -C "$(MD)" rev-parse HEAD | head -c8)

IMAGES = $(shell docker images | grep awesomeincapi | awk '{ print $$3; }')

#Dev and local

docker_image:
	docker build -t registry.gitlab.com/kaigosoftware/awesome-inc-api/awesomeincapi:${BUILD} .

clean_docker:
	docker rmi $(IMAGES)

start_minikube:
	minikube start --vm-driver=hyperkit --memory 2048 && \
	minikube addons enable ingress

deploy_to_minikube:
	helm install awesomeincapi awesomeinc-chart/ --create-namespace --namespace=awesomeinc

#Gitlab CI/CD config

test_gitlab:
	cd awesomeinc && python3 manage.py test

docker_image_gitlab:
	docker build -t registry.gitlab.com/kaigosoftware/awesome-inc-api/awesomeincapi:${BUILD} .  && \
	docker push registry.gitlab.com/kaigosoftware/awesome-inc-api/awesomeincapi:${BUILD}

deploy_gitlab:
	kubectl set image deployment/awesomeincapi awesomeincapi=registry.gitlab.com/kaigosoftware/awesome-inc-api/awesomeincapi:${BUILD} -n=${NAMESPACE} --record && \
	kubectl rollout status deployment/awesomeincapi -n=${NAMESPACE}

tag_gitlab:
	docker tag registry.gitlab.com/kaigosoftware/awesome-inc-api/awesomeincapi:${BUILD} registry.gitlab.com/kaigosoftware/awesome-inc-api/awesomeincapi:${TAG} && \
	docker push registry.gitlab.com/kaigosoftware/awesome-inc-api/awesomeincapi:${TAG}

package_helm_chart:
	helm package awesomeinc-chart && \
	mkdir awesomeincapi-helm && \
	mv awesomeincapi-*.tgz awesomeincapi-helm/ && \
	helm repo index awesomeincapi-helm/ --url https://awesomeinc-api-helm.storage.googleapis.com

push_helm_chart:
	gsutil rsync -d awesomeincapi-helm/ gs://awesomeinc-api-helm
