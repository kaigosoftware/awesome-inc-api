#build stage----------------------------
FROM python:3.9.10-alpine3.15 as builder

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

# Create the virtual environment.
RUN python3 -m venv /venv
ENV PATH=/venv/bin:$PATH

COPY awesomeinc/requirements.txt /requirements.txt

# install dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt


#final stage------------------------------
FROM python:3.9.10-alpine3.15

#Add depencencies for psycopg2
run apk update \
    && apk --no-cache add libpq

# Copy the virtual environment from the first stage.
COPY --from=builder /venv /venv
ENV PATH=/venv/bin:$PATH

COPY awesomeinc /app
WORKDIR /app

CMD ["gunicorn", "-w 2", "awesomeinc.wsgi", "-b 0.0.0.0:8000"]