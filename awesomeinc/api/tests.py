import sys
from django.test import TestCase
from rest_framework.test import APIClient
import logging



class APITest(TestCase):

    fixtures = ['api/fixtures/testdata.json',]

    log = logging.getLogger("test_logger")
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    client = APIClient()

    #test a simple get by id of installations
    def test_get_installation_api(self):
        response = self.client.get('/installations/1000/', format='json')
        self.assertEqual(response.status_code, 200)
        responseJSON = response.json()
        self.assertEqual(responseJSON['id'], 1000)

    #test a post of a new installation and a get to test the create
    def test_post_installation_api(self):
        installation = {"name": "Inst-99999", "description": "Test installation", "product": 1001, "customer": 1002,"installation_date": "2021-12-21"}
        response = self.client.post('/installations/', installation, format='json')
        self.assertEqual(response.status_code, 201)
        id = response.json()['id']
        response = self.client.get('/installations/'+str(id)+"/", format='json')
        self.assertEqual(response.status_code, 200)
        responseJSON = response.json()
        self.assertEqual(responseJSON['id'], id)

    #test a simple get by id of customers
    def test_customers_api(self):
        response = self.client.get('/customers/1000/', format='json')
        self.assertEqual(response.status_code, 200)
        responseJSON = response.json()
        self.assertEqual(responseJSON['id'], 1000)

    #test a simple get by id of products
    def test_products_api(self):
        response = self.client.get('/products/1000/', format='json')
        self.assertEqual(response.status_code, 200)
        responseJSON = response.json()
        self.assertEqual(responseJSON['id'], 1000)

        