from rest_framework import serializers
from .models import Country, Customer, Installation, Product, ProductCategory

#-----------COUNTRY SERIALIZERS----------

#Serializer for the Country model
class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ['id', 'name', 'region']

#-----------PRODUCT AND PRODUCT CATEGORY SERIALIZERS----------

#Serializer for the ProductCategory model
class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields = ['id', 'name']

#Serializer for the Product model (used for POST request)
class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'reference', 'name', 'category', 'price']

#Serializer for the Product model with nested product category model (used for GET request)
class NestedProductSerializer(serializers.ModelSerializer):
    category = ProductCategorySerializer()

    class Meta:
        model = Product
        fields = ['id', 'reference', 'name', 'category', 'price']

#-----------NESTABLE CUSTOMER SERIALIZERS----------had to be declarde before NestedInstallationSerializer

#Serializer for the Customer model when it is nested (removing the installations list)
class NestableCustomerSerialize(serializers.ModelSerializer):
    country = CountrySerializer()

    class Meta:
        model = Customer
        fields = ['id', 'name', 'email', 'country', 'premium_customer']

#-----------INSTALLATIONS SERIALIZERS----------

#Serializer for the Installation model (used for POST request)
class InstallationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Installation
        fields = ['id', 'name', 'description', 'product', 'customer', 'installation_date']

#Serializer for the Installation model with nested product and customer models (used for GET request)
class NestedInstallationSerializer(serializers.ModelSerializer):
    product =  NestedProductSerializer()
    customer = NestableCustomerSerialize()

    class Meta:
        model = Installation
        fields = ['id', 'name', 'description', 'product', 'customer', 'installation_date']

#Serializer for the Installation model when it is nested in a customer (removing the customer reference)
class NestableInstallationSerializer(serializers.ModelSerializer):
    product =  NestedProductSerializer()

    class Meta:
        model = Installation
        fields = ['id', 'name', 'description', 'product', 'installation_date']

#-----------CUSTOMER SERIALIZERS----------

#Serializer for the Customer model (used for POST request)
class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ['id', 'name', 'email', 'country', 'premium_customer', 'installations']

#Serializer for the Customer model with nested country and installation models (used for GET request)
class NestedCustomerSerializer(serializers.ModelSerializer):
    country = CountrySerializer()
    installations = NestableInstallationSerializer(many=True)

    class Meta:
        model = Customer
        fields = ['id', 'name', 'email', 'country', 'premium_customer', 'installations']