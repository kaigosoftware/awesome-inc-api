from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import CustomerViewSet, InstallationViewSet, ProductViewSet

#Adding api routes
router = DefaultRouter()
router.register(r"installations", InstallationViewSet, basename='installation')
router.register(r"customers", CustomerViewSet, basename='customer')
router.register(r"products", ProductViewSet, basename='product')

urlpatterns = [
    path("", include(router.urls))
]