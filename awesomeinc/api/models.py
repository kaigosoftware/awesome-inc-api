from django.db import models

from django.conf import settings


#Model representing a Country
class Country(models.Model):
    name = models.CharField(max_length=100)
    region = models.CharField(max_length=100)

    class Meta:
        managed = getattr(settings, 'UNDER_TEST', False)
        db_table = 'country'

#Model representing a Customer
class Customer(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    country = models.ForeignKey(Country, on_delete = models.PROTECT)
    premium_customer = models.CharField(max_length=100)

    class Meta:
        managed = getattr(settings, 'UNDER_TEST', False)
        db_table = 'customer'

#Model representing a Product Category
class ProductCategory(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        managed = getattr(settings, 'UNDER_TEST', False)
        db_table = 'product_category'

#Model representing a Product
class Product(models.Model):
    reference = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    category = models.ForeignKey(ProductCategory, on_delete = models.PROTECT, related_name='products')
    price = models.CharField(max_length=100)

    class Meta:
        managed = getattr(settings, 'UNDER_TEST', False)
        db_table = 'product'

#Model representing an Installation
class Installation(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    product = models.ForeignKey(Product, on_delete = models.PROTECT)
    customer = models.ForeignKey(Customer, on_delete = models.PROTECT, related_name='installations')
    installation_date = models.DateField()

    class Meta:
        managed = getattr(settings, 'UNDER_TEST', False)
        db_table = 'installation'
