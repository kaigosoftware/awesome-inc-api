from rest_framework import viewsets
from .models import Customer, Installation, Product
from .serializers import CustomerSerializer, InstallationSerializer, ProductSerializer, NestedCustomerSerializer, NestedInstallationSerializer, NestedProductSerializer

#Serializer mixin to use different serializer for GET and POST
class NestedSerializerMixin(viewsets.ModelViewSet):
    read_serializer_class = None

    def get_serializer_class(self):
        if self.request.method.lower() == "get":
            return self.read_serializer_class
        return self.serializer_class

#ViewSet for the installations api
class InstallationViewSet(NestedSerializerMixin):
    serializer_class = InstallationSerializer
    read_serializer_class = NestedInstallationSerializer
    queryset = Installation.objects.all()

#ViewSet for the customers api
class CustomerViewSet(NestedSerializerMixin):
    serializer_class = CustomerSerializer
    read_serializer_class = NestedCustomerSerializer
    queryset = Customer.objects.all()

#ViewSet for the products api
class ProductViewSet(NestedSerializerMixin):
    serializer_class = ProductSerializer
    read_serializer_class = NestedProductSerializer
    queryset = Product.objects.all()