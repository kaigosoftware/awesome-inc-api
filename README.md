# Awesome Inc Data API


## Goals

We would like to give access to Awesome Inc data using an API.

## Requirements

- [ ] Implement a Python REST or GraphQL API on top of Awesome Inc database;
- [ ] Test the API;
- [ ] Containerize the application;
- [ ] Version your code;
- [ ] Create an Helm chart to deploy the application on Kubernetes (bonus);
- [ ] Create a CI/CD pipeline (bonus)